<?php

$timenow  = date('Y-m-d H:i:s');
$resource = fopen('ipn.log', 'a');
$response = "[$timenow]" . "\r\n";

// STEP 1: read POST data
$raw_post_data = file_get_contents('php://input' );
$raw_post_array = explode( '&', $raw_post_data );
$myPost = array();

foreach ( $raw_post_array as $keyval ) {
  $keyval = explode ( '=', $keyval );
  if ( count( $keyval ) == 2 ) {
    $myPost[ $keyval[ 0 ] ] = urldecode( $keyval[ 1 ] );
  }
}

// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';

if (function_exists('get_magic_quotes_gpc')) {
  $get_magic_quotes_exists = true;
}

foreach ( $myPost as $key => $value ) {
  if ( $get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1 ) {
    $value = urlencode( stripslashes( $value ) );
  } else {
    $value = urlencode( $value );
  }
  $req .= "&$key=$value";
}

// Step 2: POST IPN data back to PayPal to validate
$ch = curl_init( 'https://ipnpb.paypal.com/cgi-bin/webscr' );

curl_setopt( $ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1 );
curl_setopt( $ch, CURLOPT_POST, 1 );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt( $ch, CURLOPT_POSTFIELDS, $req );
curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 1 );
curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 2 );
curl_setopt( $ch, CURLOPT_FORBID_REUSE, 1 );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array( 'Connection: Close' ) );
curl_setopt( $ch, CURLOPT_CAINFO, dirname( __FILE__ ) . '/cacert.pem' );

if ( ! ( $res = curl_exec( $ch ) ) ) {
  $response = 'Curl error = ' . curl_error($ch);
} else {
	if ( strcmp($res, "VERIFIED") == 0 ) {
		foreach ($myPost as $key => $value) {
			$response .= $key . " = " . $val . "\r\n";
		}
	}
}

$response .= "\r\n";
fwrite($resource, $response);
fclose($resource);
curl_close($ch);
exit;
