<?php

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Common\PayPalModel;
use PayPal\Rest\ApiContext;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\PaymentExecution;
use PayPal\Api\ShippingAddress;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PatchRequest;
use PayPal\Api\Transaction;
use PayPal\Api\InputFields;
use PayPal\Api\WebProfile;
use PayPal\Api\Agreement;
use PayPal\Api\ItemList;
use PayPal\Api\Currency;
use PayPal\Api\Payment;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Patch;
use PayPal\Api\Item;
use PayPal\Api\Plan;

/**
 * Class Paypal
 *
 * Can be used to make a purchase
 */
class Paypal
{
    /**
     * @var PayPal\Rest\ApiContext $apiContext merchant authentication context
     */
    private $apiContext;

    /**
     * @var string $returnurl redirect url for not canceled payments
     */
    public  $returnurl;

    /**
     * @var string $cancelurl redirect url for canceled payments
     */
    public  $cancelurl;

    /**
     * @var string $currency currency that will be used during the purchase
     */
    static  $currency = 'USD';

    /**
     * @var bool $sandboxEnabled used to define the environment
     */
    static  $sandboxEnabled = false;

    /**
     * @var string SUBSCRIPTIONTYPE subscription product type
     */
    const   SUBSCRIPTIONTYPE = 'subscription';

    /**
     * @var string SINGLETYPE single payment product type
     */
    const   SINGLETYPE = 'single';

    /**
     * Construct
     *
     * @param  string $clientId     client id from PayPal credentials
     * @param  string $clientSecret client secret from PayPal credentials
     */
    public function __construct( string $clientId, string $clientSecret )
    {
        $this->setApiContext( $clientId, $clientSecret );
    }

    /**
     * Get Response object
     *
     * @param  bool   $status
     * @param  string $message
     * @param  array  $data
     *
     * @return object $response
     */
    private function getResponse( bool $status, string $message = '', array $data = [] )
    {
        $response = (object)[
            'status'  => $status ? 'success' : 'error',
            'message' => $message,
            'data'    => $data
        ];

        return $response;
    }

    /**
     * Get shipping address object
     *
     * @param  array  $customer
     * @param  bool   $agreement
     *
     * @return PayPal\Api\ShippingAddress $shippingAddress
     */
    private function getShippingAddress( array $customer, bool $agreement )
    {
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setCity( $customer[ 'shipping_city' ] );
        $shippingAddress->setCountryCode( $customer[ 'shipping_country' ] );
        $shippingAddress->setPostalCode( $customer[ 'shipping_zip' ] );
        $shippingAddress->setLine1( $customer[ 'shipping_address1' ] );
        $shippingAddress->setLine2( $customer[ 'shipping_address2' ] );
        $shippingAddress->setPhone( $customer[ 'shipping_phone' ] );
        $shippingAddress->setState( $customer[ 'shipping_state' ] );

        if ( ! $agreement ) {
            $customerName = $customer[ 'first_name' ] . ' ' . $customer[ 'last_name' ];
            $shippingAddress->setRecipientName( $customerName );
        }

        return $shippingAddress;
    }

    /**
     * Create billing plan
     *
     * @param  object          $itemDetails
     *
     * @return PayPal\Api\Plan $plan
     */
    private function createPlan( object $itemDetails )
    {
        $plan = new Plan();
        $plan->setName( $itemDetails->name )
            ->setDescription( $itemDetails->description ?? $itemDetails->name )
            ->setType( 'INFINITE' );

        $paymentDefinition = new PaymentDefinition();

        $paymentDefinition->setName( 'Regular Payments' )
            ->setType( 'REGULAR' )
            ->setFrequency( 'Month' )
            ->setFrequencyInterval( '1' )
            ->setCycles( 0 )
            ->setAmount(
                new Currency(
                    [
                        'value' => $itemDetails->price,
                        'currency' => self::$currency
                    ]
                )
            );

        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setAutoBillAmount( 'yes' )
            ->setInitialFailAmountAction( 'CONTINUE' )
            ->setMaxFailAttempts( '0' )
            ->setSetupFee(
                new Currency(
                    [
                        'value' => 1,
                        'currency' => self::$currency
                    ]
                )
            );
        if ( !empty( $this->returnurl ) ) {
            $merchantPreferences->setReturnUrl( $this->returnurl );
        }
        if ( !empty( $this->cancelurl ) ) {
            $merchantPreferences->setCancelUrl( $this->cancelurl );
        }

        $plan->setPaymentDefinitions( [ $paymentDefinition ] );
        $plan->setMerchantPreferences( $merchantPreferences );

        try {
            $plan = $plan->create( $this->apiContext );
        } catch ( Exception $ex ) {
            $plan = false;
        }

        return $plan;
    }

    /**
     * Update billing plan
     *
     * @param  PayPal\Api\Plan $plan
     *
     * @return PayPal\Api\Plan $plan
     */
    private function updatePlan( Plan $plan )
    {
        try {
            $patch = new Patch();
            $value = new PayPalModel( '{
                "state":"ACTIVE"
            }' );

            $patch->setOp( 'replace' )
                ->setPath( '/' )
                ->setValue( $value );

            $patchRequest = new PatchRequest();
            $patchRequest->addPatch( $patch );
            $plan->update( $patchRequest, $this->apiContext );
            $plan = Plan::get( $plan->getId(), $this->apiContext );
        } catch ( Exception $ex ) {
            $plan = false;
        }

        return $plan;
    }

    /**
     * Process subscription payment, create billing agreement for the subscription
     *
     * @param  object $itemDetails
     * @param  array  $customer
     * @param  string $referenceTransaction
     *
     * @return object $response return response object on failure and redirect to approval link on success
     */
    private function processSubscriptionPayment( object $itemDetails, array $customer, string $referenceTransaction = '' )
    {
        $agreement = new Agreement();
        $agreement->setName( 'Subscription Agreement' )
            ->setDescription( 'Subscription Agreement' )
            ->setStartDate( date( 'c', time() ) );

        $createdPlan = $this->createPlan( $itemDetails );
        if ( ! $createdPlan ) {
            $response = $this->getResponse( false, 'Failed to create a plan', [ 'item' => $itemDetails ] );
            return $response;
        }

        $updatedPlan = $this->updatePlan($createdPlan);
        if ( ! $updatedPlan ) {
            $response = $this->getResponse( false, 'Failed to update the plan', [ 'item' => $itemDetails ] );
            return $response;
        }

        $plan = new Plan();
        $plan->setId( $updatedPlan->getId() );
        $agreement->setPlan( $plan );

        $payer = new Payer();
        $payer->setPaymentMethod( 'paypal' );
        $agreement->setPayer( $payer );

        $shippingAddress = $this->getShippingAddress( $customer, true );
        $agreement->setShippingAddress( $shippingAddress );

        try {
            $agreement = $agreement->create( $this->apiContext );
            $approvalUrl = $agreement->getApprovalLink();
            header( 'Location: ' . $approvalUrl );
            exit( 0 );
        } catch ( Exception $ex ) {
            $response = $this->getResponse( false, 'Failed to create an agreement', [ 'item' => $itemDetails ] );
            return $response;
        }
    }

    /**
     * Process one time payment
     *
     * @param  array  $items
     * @param  array  $customer
     * @param  string $referenceTransaction
     *
     * @return object $response return response object on failure and redirect to approval link on success
     */
    private function processOneTimePayment( array $items, array $customer, string $referenceTransaction = '' )
    {
        $payer = new Payer();
        $payer->setPaymentMethod( 'paypal' );

        $total = 0;
        $itemsList = [];

        foreach ( $items as $i => $itemDetails ) {
            $item = new Item();
            $item->setName( $itemDetails->name )
                ->setCurrency( self::$currency )
                ->setQuantity( $itemDetails->quantity ?? 1 )
                ->setPrice( $itemDetails->price );
            $itemsList[] = $item;
            $total += (float)$itemDetails->price;
        }

        $itemList = new ItemList();
        $itemList->setItems( $itemsList );

        $shippingAddress = $this->getShippingAddress( $customer, true );
        $itemList->setShippingAddress( $shippingAddress );

        $amount = new Amount();
        $amount->setCurrency( self::$currency )
            ->setTotal( $total );

        $transaction = new Transaction();
        $transaction->setAmount( $amount )
            ->setItemList( $itemList )
            ->setDescription( 'Purchase' )
            ->setInvoiceNumber( uniqid() );
        if ( ! empty( $referenceTransaction ) ) {
            $transaction->setPurchaseUnitReferenceId( $referenceTransaction );
        }

        $redirectUrls = new RedirectUrls();
        if ( !empty( $this->returnurl ) ) {
            $redirectUrls->setReturnUrl( $this->returnurl );
        }
        if ( !empty( $this->cancelurl ) ) {
            $redirectUrls->setCancelUrl( $this->cancelurl );
        }

        $payment = new Payment();
        $payment->setIntent( 'sale' )
            ->setPayer( $payer )
            ->setRedirectUrls( $redirectUrls )
            ->setTransactions( [ $transaction ] );

        try {
            $payment->create( $this->apiContext );
            $approvalUrl = $payment->getApprovalLink();
            header( 'Location: ' . $approvalUrl );
            exit( 0 );
        } catch ( Exception $ex ) {
            $response = $this->getResponse( false, 'Failed to create a payment' );
            return $response;
        }
    }

    /**
     * Set api context ( merchant authentication details )
     *
     * @param  string $clientId     client id from PayPal credentials
     * @param  string $clientSecret client secret from PayPal credentials
     */
    public function setApiContext( string $clientId, string $clientSecret )
    {
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $this->apiContext->setConfig( [
            'mode' => self::$sandboxEnabled ? 'sandbox' : 'live',
            'log.LogEnabled' => true,
            'log.FileName' => './paypal.log',
            'log.LogLevel' => self::$sandboxEnabled ? 'DEBUG' : 'INFO',
            'cache.enabled' => true
        ] );
    }

    /**
     * Process purchase ( process one time payment or subscrption payment )
     *
     * @param  array  $items
     * @param  array  $customer
     * @param  string $referenceTransaction
     *
     * @return object $response return response object on failure and redirect to payment approval link on successful completion
     */
    public function purchase( array $items, array $customer, string $referenceTransaction = '' )
    {
        $itemTypes = [];
        foreach ( $items as $i => $itemDetails ) {
            $itemTypes[] = $itemDetails->type;
        }
        $itemTypes = array_unique( $itemTypes );

        if ( count( $itemTypes ) !== 1 ) {
            $response = $this->getResponse( false, 'Cannot process payment for mixed products' );
        } else {
            if ( $itemTypes[0] !== self::SINGLETYPE && $itemTypes[0] !== self::SUBSCRIPTIONTYPE ) {
                $response = $this->getResponse( false, 'Unknown type of product' );
            } else {
                if ( $itemTypes[0] === self::SUBSCRIPTIONTYPE ) {
                    if ( count( $items ) > 1 ) {
                        $response = $this->getResponse( false, 'Cannot process payment for multiple subscriptions' );
                    } else {
                        $response = $this->processSubscriptionPayment( $items[0], $customer, $referenceTransaction );
                    }
                } else {
                    $response = $this->processOneTimePayment( $items, $customer, $referenceTransaction );
                }
            }
        }

        return $response;
    }

    /**
     * Execute one time payment
     *
     * @param  string $paymentId
     * @param  string $payerId
     *
     * @return object $response
     */
    public function executeOneTimePayment( string $paymentId, string $payerId )
    {
        $payment = Payment::get( $paymentId, $this->apiContext );

        $execution = new PaymentExecution();
        $execution->setPayerId( $payerId );

        try {
            $result = $payment->execute( $execution, $this->apiContext );
            try {
                $payment = Payment::get( $paymentId, $this->apiContext );
                $response = $this->getResponse( true, 'Payment executed', [ 'paymentId' => $paymentId ] );
            } catch ( Exception $ex ) {
                $response = $this->getResponse( false, 'Failed to get the payment', [ 'paymentId' => $paymentId ] );
            }
        } catch ( Exception $ex ) {
            $response = $this->getResponse( false, 'Failed to execute the payment', [ 'paymentId' => $paymentId ] );
        }

        return $response;
    }

    /**
     * Execute subscription payment
     *
     * @param  string $token
     *
     * @return object $response
     */
    public function executeSubscriptionPayment( string $token )
    {
        $agreement = new \PayPal\Api\Agreement();

        try {
            $agreement->execute( $token, $this->apiContext );
            $response = $this->getResponse( true, 'Agreement executed', [ 'token' => $token ] );
            try {
                $agreement = \PayPal\Api\Agreement::get( $agreement->getId(), $this->apiContext );
            } catch ( Exception $ex ) {
                $response = $this->getResponse( false, 'Failed to get the agreement', [ 'token' => $token ] );
            }
        } catch ( Exception $ex ) {
            $response = $this->getResponse( false, 'Failed to execute the agreement', [ 'token' => $token ] );
        }

        return $response;
    }
}
