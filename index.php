<?php 

require_once('./PayPalSDK/vendor/autoload.php');
require_once('./Paypal.php');

// https://developer.paypal.com/developer/applications/
// "REST API apps" section
define('PAYPAL_CLIENT_ID', 'ENTER_YOUR_PAYPAL_APP_CLIENT_ID_HERE');
define('PAYPAL_CLIENT_SECRET', 'ENTER_YOUR_PAYPAL_APP_CLIENT_SECRET_HERE');

// enable sandbox
Paypal::$sandboxEnabled = true;

$base_url = ( ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "off" ) ? "https" : "http" ) . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$base_url = substr( $base_url, 0, strpos( $base_url, '?' ) );

if ( isset($_GET['type']) ) {

    $paypal = new Paypal( PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET );

    if ( $_GET['type'] == 'single' ) {
        if ( isset($_GET['success']) ) {

            require_once('./purchaseTestNormalProduct.php');
            exit(0);

        } else {

            // test normal product payment

            $items = [
                (object)[
                    'name'  => 'Test-Item-1',
                    'price' => '12',
                    'type' => Paypal::SINGLETYPE
                ],
                (object)[
                    'name'  => 'Test-Item-2',
                    'price' => '4.4',
                    'type' => Paypal::SINGLETYPE
                ],
                (object)[
                    'name'  => 'Test-Item-3',
                    'price' => '5.55',
                    'type' => Paypal::SINGLETYPE
                ]
            ];

            $customer = [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'shipping_city' => 'Claymont',
                'shipping_country' => 'US',
                'shipping_zip' => '19703',
                'shipping_address1' => '950 Ridge RD C25',
                'shipping_address2' => 'C6882',
                'shipping_state' => 'DE',
                'shipping_phone' => '1234567890'
            ];

            $paypal->returnurl = $base_url . '/index.php?type=single&success=true';
            $paypal->cancelurl = $base_url . '/index.php?type=single&success=false';

            $res = $paypal->purchase($items, $customer);

            echo $res->message;
            exit(0);

        }
    } else if ( $_GET['type'] == 'subscription' ) {
        if ( isset($_GET['success']) ) {

            require_once('./purchaseTestSubscriptionProduct.php');
            exit(0);

        } else {

            // test subscription payment

            $items = [
                (object)[
                    'name'  => 'Test-Item-1',
                    'price' => '10',
                    'type' => Paypal::SUBSCRIPTIONTYPE
                ],
                // no possibility to proccess subscription payment by one transaction
                // (object)[
                //  'name'  => 'Test-Item-2',
                //  'price' => '25',
                //  'type' => Paypal::SUBSCRIPTIONTYPE
                // ],
                // (object)[
                //  'name'  => 'Test-Item-3',
                //  'price' => '7.99',
                //  'type' => Paypal::SUBSCRIPTIONTYPE
                // ]
            ];

            $customer = [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'shipping_city' => 'Claymont',
                'shipping_country' => 'US',
                'shipping_zip' => '19703',
                'shipping_address1' => '950 Ridge RD C25',
                'shipping_address2' => 'C6882',
                'shipping_state' => 'DE',
                'shipping_phone' => '1234567890'
            ];

            $paypal->returnurl = $base_url . '/index.php?type=subscription&success=true';
            $paypal->cancelurl = $base_url . '/index.php?type=subscription&success=false';

            // In the paypal payment page user will see a shipping address, which is not possible to remove with the current version of sdk ( https://github.com/paypal/PayPal-REST-API-issues/issues/4 )

            $res = $paypal->purchase($items, $customer);

            echo $res->message;
            exit(0);
        }
    }
} else {

    echo "
        <p>Test normal product purchase: <button><a href='$base_url?type=single'>Test single payment</a></button></p>
        <p>Test subscription purchase: <button><a href='$base_url?type=subscription'>Test subscription payment</a></button></p>
    ";

}
